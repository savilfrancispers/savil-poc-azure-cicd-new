﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NtgOnboardingPortalApis.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NtgOnboardingPortalApis.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TeamMemberController : ControllerBase
    {


        private readonly ILogger<TeamMemberController> _logger;

        public TeamMemberController(ILogger<TeamMemberController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<TeamMember> Get()
        {
            return GetTeamData();
        }



        private IEnumerable<TeamMember> GetTeamData()
        {
            return new List<TeamMember>
            {
                new TeamMember
                {
                    Id=181477,
                    FirstName="Savil E",
                    LastName="Francis",
                    Email="savil.francis@cognizant.com"

                },

                new TeamMember
                {
                    Id=787206,
                    FirstName="Abhinav ",
                    LastName="Jalla",
                    Email="Abhinav.Jalla@cognizant.com"

                },

                new TeamMember
                {
                    Id=887030,
                    FirstName="Sathish",
                    LastName="Raghavan",
                    Email="Sathish.Raghavan@cognizant.com"

                },

                new TeamMember
                {
                    Id=443437,
                    FirstName="Amritha",
                    LastName="Antony",
                    Email="Amritha.Antony@cognizant.com"

                }
            };
        }
    }
}

